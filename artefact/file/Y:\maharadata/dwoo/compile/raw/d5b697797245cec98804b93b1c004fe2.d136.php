<?php
/* template head */
if (function_exists('PluginLoadquota')===false)
	$this->getLoader()->loadPlugin('PluginLoadquota');
if (function_exists('PluginStr')===false)
	$this->getLoader()->loadPlugin('PluginStr');
if (function_exists('PluginContextualhelp')===false)
	$this->getLoader()->loadPlugin('PluginContextualhelp');
/* end template head */ ob_start(); /* template body */ ;
echo PluginLoadquota($this);?>

<div class="card">
    <h3 class="card-header">
        <?php echo PluginStr($this, "quota", 'mahara', null, null, null, null, null);?>

        <span class="float-right">
        <?php echo PluginContextualhelp($this, 'artefact', 'file', null, null, 'quota_message', null);?>

        </span>
    </h3>
    <div class="card-body">
        <p id="quota_message">
            <?php echo (isset($this->scope["QUOTA_MESSAGE"]) ? $this->scope["QUOTA_MESSAGE"] : null);?>

        </p>
        <div id="quotawrap" class="progress">
            <div id="quota_fill" class="progress-bar <?php if ((isset($this->scope["QUOTA_PERCENTAGE"]) ? $this->scope["QUOTA_PERCENTAGE"] : null) < 11) {
?>small-progress<?php 
}?>" role="progressbar" aria-valuenow="<?php if ((isset($this->scope["QUOTA_PERCENTAGE"]) ? $this->scope["QUOTA_PERCENTAGE"] : null)) {

echo (is_string($tmp=$this->scope["QUOTA_PERCENTAGE"]) ? htmlspecialchars($tmp, ENT_QUOTES, $this->charset) : $tmp);

}
else {
?>0<?php 
}?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo (is_string($tmp=$this->scope["QUOTA_PERCENTAGE"]) ? htmlspecialchars($tmp, ENT_QUOTES, $this->charset) : $tmp);?>%;">
                <span><?php echo (is_string($tmp=$this->scope["QUOTA_PERCENTAGE"]) ? htmlspecialchars($tmp, ENT_QUOTES, $this->charset) : $tmp);?>%</span>
            </div>
        </div>
    </div>
</div>
<?php  /* end template body */
return $this->buffer . ob_get_clean();
?>