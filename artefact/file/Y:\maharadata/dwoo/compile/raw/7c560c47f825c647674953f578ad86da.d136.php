<?php
/* template head */
if (class_exists('Dwoo\Plugins\Functions\PluginInclude')===false)
	$this->getLoader()->loadPlugin('PluginInclude');
if (function_exists('PluginStr')===false)
	$this->getLoader()->loadPlugin('PluginStr');
/* end template head */ ob_start(); /* template body */ ;
echo $this->classCall('Dwoo\Plugins\Functions\Plugininclude',
                        array("header.tpl", null, null, null, '_root', null));?>

    <?php echo (isset($this->scope["settingsformtag"]) ? $this->scope["settingsformtag"] : null);?>

    <div class="table-responsive">
        <table id="profileicons" class="d-none fullwidth table table-striped">
            <thead>
                <tr>
                    <th class="profileiconcell"><?php echo PluginStr($this, "image", 'mahara', null, null, null, null, null);?></th>
                    <th><?php echo PluginStr($this, "imagetitle", 'artefact.file', null, null, null, null, null);?></th>
                    <th class="text-center"><?php echo PluginStr($this, "Default", 'artefact.file', null, null, null, null, null);?></th>
                    <th class="text-center"><?php echo PluginStr($this, "delete", 'mahara', null, null, null, null, null);?></th>
                </tr>
            </thead>
            <tbody>
            </tbody>
            <tfoot>
                <tr>
                    <td colspan="4" class="text-right">
                        <button id="settings_default" type="submit" class="submit btn btn-secondary" name="default">
                            <span class="icon icon-check icon-lg text-success left" role="presentation" aria-hidden="true"></span>
                            <?php echo PluginStr($this, "setdefault", 'artefact.file', null, null, null, null, null);?>

                        </button>
                        <button id="settings_delete" type="submit" class="delete btn btn-secondary" name="delete">
                             <span class="icon icon-trash-alt icon-lg text-danger left" role="presentation" aria-hidden="true"></span>
                             <?php echo PluginStr($this, "deleteselectedicons", 'artefact.file', null, null, null, null, null);?>

                         </button>
                    </td>
                </tr>
            </tfoot>
        </table>
    </div>
    <input type="hidden" name="pieform_settings" value="">
    <input type="hidden" name="sesskey" value="<?php echo (is_string($tmp=$this->scope["USER"]->get('sesskey')) ? htmlspecialchars($tmp, ENT_QUOTES, $this->charset) : $tmp);?>">
    </form>

    <h2><?php echo PluginStr($this, "uploadprofileicon", "artefact.file", null, null, null, null, null);?></h2>
    <p class="lead">
        <?php echo PluginStr($this, "profileiconsiconsizenotice", "artefact.file", (is_string($tmp=(isset($this->scope["imagemaxdimensions"]) ? $this->scope["imagemaxdimensions"] : null)) ? htmlspecialchars($tmp, ENT_QUOTES, $this->charset) : $tmp), null, null, null, null);?>

    </p>

    <?php echo (isset($this->scope["uploadform"]) ? $this->scope["uploadform"] : null);?>

<?php echo $this->classCall('Dwoo\Plugins\Functions\Plugininclude',
                        array("footer.tpl", null, null, null, '_root', null));?>

<?php  /* end template body */
return $this->buffer . ob_get_clean();
?>