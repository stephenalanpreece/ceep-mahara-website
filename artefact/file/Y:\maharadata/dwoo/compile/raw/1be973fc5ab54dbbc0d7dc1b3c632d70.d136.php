<?php
/* template head */
if (class_exists('Dwoo\Plugins\Functions\PluginInclude')===false)
	$this->getLoader()->loadPlugin('PluginInclude');
if (function_exists('PluginStr')===false)
	$this->getLoader()->loadPlugin('PluginStr');
if (function_exists('PluginContextualhelp')===false)
	$this->getLoader()->loadPlugin('PluginContextualhelp');
/* end template head */ ob_start(); /* template body */ ;
echo $this->classCall('Dwoo\Plugins\Functions\Plugininclude',
                        array("header.tpl", null, null, null, '_root', null));?>


<div class="row">
<div class="col-md-3 login-card">
	<div class="card">
	    <h3 class="card-header">
	        <?php echo PluginStr($this, "login", 'mahara', null, null, null, null, null);?>

	        <span class="float-right"><?php echo PluginContextualhelp($this, 'core', 'core', null, null, 'loginbox', null);?></span>
	    </h3>
	    <div class="card-body">
	        <noscript><p><?php echo PluginStr($this, "javascriptnotenabled", 'mahara', null, null, null, null, null);?></p></noscript>
	        <?php if($doCache) {
	echo '<dwoo:dynamic_'.$dynamicId.'><?php echo (isset($this->scope["messages"]["loginbox"]) ? $this->scope["messages"]["loginbox"]:null);?></dwoo:dynamic_'.$dynamicId.'>';
} else {
	echo (isset($this->scope["messages"]["loginbox"]) ? $this->scope["messages"]["loginbox"]:null);
}?>

	        <div id="loginform_container">
	            <?php echo (isset($this->scope["login_form"]) ? $this->scope["login_form"] : null);?>

	        </div>
	    </div>
	</div>
</div>
</div>

<?php echo $this->classCall('Dwoo\Plugins\Functions\Plugininclude',
                        array("footer.tpl", null, null, null, '_root', null));?>

<?php  /* end template body */
return $this->buffer . ob_get_clean();
?>